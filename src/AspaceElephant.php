<?php

/*
 * (c) Justin Dalton <jcdalton@wm.edu>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE
 */

namespace wmlibraries\AspaceElephant;

use http\Exception\InvalidArgumentException;

class AspaceElephant {
  private $dotenv;
  protected $strBaseURL;
  protected $strUsername;
  protected $strPassword;
  protected $strAspaceSession;
  protected $arrHeader;

  /**
   * AspaceElephant constructor.
   *
   * @param string $strBaseURL
   * @param string $strUsername
   * @param string $strPassword
   *
   * @throws \Exception
   */
  public function __construct($strBaseURL, $strUsername, $strPassword) {
    if(filter_var($strBaseURL, FILTER_VALIDATE_URL)) {
      $this->strBaseURL = $strBaseURL;
    } else {
      throw new \InvalidArgumentException('The URL must be valid.');
    }
    if(is_string($strUsername) && $strUsername != '') {
      $this->strUsername = $strUsername;
    } else {
      throw new \InvalidArgumentException('Username must not be empty.');
    }
    if(is_string($strPassword) && $strPassword != '') {
      $this->strPassword = $strPassword;
    } else {
      throw new \InvalidArgumentException('Password must not be empty.');
    }


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/users/" . $this->strUsername . "/login");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "password=".$this->strPassword);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    //print_r($server_output);
    if($server_output != false) {
      $arrReturn = json_decode($server_output);
      $this->strAspaceSession = $arrReturn->session;
      $this->arrHeader = ['X-ArchivesSpace-Session:' . $this->strAspaceSession];
      curl_close($ch);
    } else {
      throw new \Exception($this->strBaseURL . ' returned failure.');
    }

  }

  /**
   * @param int $intRepoId
   * Gets array of ids representing all of the resources in the repository
   * By default queries on Repo 2 because that is what almost everyone uses.
   * @return array
   * @throws \InvalidArgumentException
   */
  public function getResources($intRepoId = 2) {
    if(is_numeric($intRepoId) && $intRepoId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/resources?all_ids=true");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $all_resources = curl_exec($ch);
      $arrAllResources = json_decode($all_resources);
      //print_r($arrAllResources);
      curl_close($ch);
      return $arrAllResources;
    } else {
      throw new \InvalidArgumentException('$intRepoId must be a positive numeric value');
    }
  }

  /**
   * @param int $intId
   * @param int $intRepoId
   * Takes a resource id and uses a get request to
   * return a php object representation of an ArchivesSpace resource
   *
   * @return object
   * @throws \InvalidArgumentException
   */
  public function getResource($intId, $intRepoId = 2) {
    if(is_numeric($intId) && $intId > 0) {
      if(is_numeric($intRepoId) && $intRepoId > 0) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
        curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/resources/$intId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $resource = curl_exec($ch);
        $objResource = json_decode($resource);
        //print_r($arrAllResources);
        curl_close($ch);
        return $objResource;
      } else {
        throw new \InvalidArgumentException('$intRepoId must be a positive numeric value');
      }
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

//  /**
//   * @param $objResource Object
//   * Takes a PHP object representation of an ArchivesSpace resource
//   * converts it to the JSON representation and posts the query.
//   * @throws \InvalidArgumentException
//   * @throws \Exception
//   */
//  public function updateResource($objResource) {
//    if(is_object($objResource)) {
//      $intResourceId = $objResource->id;
//      $updatedJsonResource = json_encode($objResource);
//      $chUpdate = curl_init();
//      curl_setopt($chUpdate, CURLOPT_HTTPHEADER, $this->arrHeader);
//      curl_setopt($chUpdate, CURLOPT_RETURNTRANSFER, true);
//      curl_setopt($chUpdate, CURLOPT_URL, $this->strBaseURL . "/repositories/2/resources/$intResourceId");
//      curl_setopt($chUpdate, CURLOPT_POST, 1);
//      curl_setopt($chUpdate, CURLOPT_POSTFIELDS, $updatedJsonResource);
//      $updateResult = curl_exec($chUpdate);
//      curl_close($chUpdate);
//      if($updateResult != false) {
//        print_r($updateResult);
//      } else {
//        throw new \Exception('Error: Could not update Resource');
//      }
//    } else {
//      throw new InvalidArgumentException('$objResource must be an object');
//    }
//  }

  /**
   * @return array
   * Gets array of ids representing all of the corporate entities in ArchivesSpace
   */
  public function getCorporateEntities() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/agents/corporate_entitites?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $all_corporate_entities = curl_exec($ch);
    curl_close($ch);
    if($all_corporate_entities != false) {
      $arrAllCorporateEntities = json_decode($all_corporate_entities);
    } else {
      throw new \Exception('Error: Could not query corporate entities');
    }
    return $arrAllCorporateEntities;
  }

  /**
   * @param int $intId
   *
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getCorporateEntity($intId) {
    if(is_numeric($intId) && $intId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/agents/corporate_entities/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $corporate_entity = curl_exec($ch);
      curl_close($ch);
      if($corporate_entity != false) {
        $objCorporateEntity = json_decode($corporate_entity);
      } else {
        throw new \Exception('Error: Could not query entity');
      }
      return $objCorporateEntity;
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

//  /**
//   * @param $objCorporateEntity object
//   * Takes a PHP object representation of an ArchivesSpace corporate entity
//   * and converts it to the JSON representation and posts the query
//   * @return bool
//   * @throws \Exception
//   * @throws \InvalidArgumentException
//   */
//  public function updateCorporateEntity($objCorporateEntity) {
//    if(is_object($objCorporateEntity)) {
//      $intCorporateEntityId = $objCorporateEntity->id;
//      $updatedJsonCorporateEntity = json_encode($objCorporateEntity);
//      $chUpdate = curl_init();
//      curl_setopt($chUpdate, CURLOPT_HTTPHEADER, $this->arrHeader);
//      curl_setopt($chUpdate, CURLOPT_RETURNTRANSFER, true);
//      curl_setopt($chUpdate, CURLOPT_URL, $this->strBaseURL . "/agents/corporate_entities/$objCorporateEntity");
//      curl_setopt($chUpdate, CURLOPT_POST, 1);
//      curl_setopt($chUpdate, CURLOPT_POSTFIELDS, $updatedJsonCorporateEntity);
//      $updateResult = curl_exec($chUpdate);
//      curl_close($chUpdate);
//      if($updateResult != false) {
//        return true;
//      } else {
//        throw new \Exception('Error: Could not update Corporate Entity');
//      }
//    } else {
//      throw new InvalidArgumentException('$objResource must be an object');
//    }
//  }

//  /**
//   * @param $intId int
//   * Takes an integer and sends a delete query for the Corporate Entity
//   * @return bool
//   * @throws \Exception
//   * @throws \InvalidArgumentException
//   */
//  public function deleteCorporateEntity($intId) {
//    if(is_numeric($intId) && $intId > 0) {
//      $curl_delete = curl_init();
//      curl_setopt($curl_delete, CURLOPT_HTTPHEADER, $this->arrHeader);
//      curl_setopt($curl_delete, CURLOPT_RETURNTRANSFER, true);
//      curl_setopt($curl_delete, CURLOPT_URL, $this->strBaseURL . "/agents/corporate_entities/$intId");
//      curl_setopt($curl_delete, CURLOPT_CUSTOMREQUEST, "DELETE");
//      $deleteResult = curl_exec($curl_delete);
//      if($deleteResult != false) {
//        return true;
//      } else {
//        throw new \Exception('Error: Could not delete Corporate Entity');
//      }
//    } else {
//      throw new \InvalidArgumentException('intId must be a positive integer');
//    }
//  }

  /**
   * @return array
   * Gets an array of all family agent ids
   * @throws \Exception
   */
  public function getFamilyAgents() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/agents/families?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $all_families = curl_exec($ch);
    curl_close($ch);
    if($all_families != false) {
      $arrAllFamilies = json_decode($all_families);
    } else {
      throw new \Exception('Error: Could not query families.');
    }

    return $arrAllFamilies;
  }

  /**
   * @param int $intId
   *
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getFamilyAgent($intId) {
    if(is_numeric($intId) && $intId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/agents/families/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $family = curl_exec($ch);
      curl_close($ch);
      if($family != false) {
        $objFamily = json_decode($family);
      } else {
        throw new \Exception('Error: Could not query family');
      }
      return $objFamily;
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

  /**
   * @return array
   * Gets an array of all the person agent ids
   * @throws \Exception
   */
  public function getPersonAgents() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/agents/people?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $all_people = curl_exec($ch);
    curl_close($ch);
    if($all_people != false) {
      $arrAllPeople = json_decode($all_people);
    } else {
      throw new \Exception('Error: Could not query families.');
    }

    return $arrAllPeople;
  }

  /**
   * @param int $intId
   * Returns the PHP object representation of an agent person
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getPersonAgent($intId) {
    if(is_numeric($intId) && $intId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/agents/people/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $person = curl_exec($ch);
      curl_close($ch);
      if($person != false) {
        $objPerson = json_decode($person);
      } else {
        throw new \Exception('Error: Could not query person');
      }
      return $objPerson;
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

  /**
   * @return array
   * Gets an array of all software agent ids
   * @throws \Exception
   */
  public function getSoftwareAgents() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/agents/software?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $all_software = curl_exec($ch);
    curl_close($ch);
    if($all_software != false) {
      $arrAllSoftware = json_decode($all_software);
    } else {
      throw new \Exception('Error: Could not software agents.');
    }

    return $arrAllSoftware;
  }

  /**
   * @param int $intId
   * Gets a PHP object representation of a software agent
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getSoftwareAgent($intId) {
    if(is_numeric($intId) && $intId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/agents/software/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $software = curl_exec($ch);
      curl_close($ch);
      if($software != false) {
        $objSoftware = json_decode($software);
      } else {
        throw new \Exception('Error: Could not query software.');
      }
      return $objSoftware;
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

  /**
   * @return array
   * Returns an array of all container profile ids
   * @throws \Exception
   */
  public function getContainerProfiles() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/container_profiles?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $all_container_profiles = curl_exec($ch);
    curl_close($ch);
    if($all_container_profiles != false) {
      $arrAllContainerProfiles = json_decode($all_container_profiles);
    } else {
      throw new \Exception('Error: Could not query container profiles.');
    }

    return $arrAllContainerProfiles;
  }

  /**
   * @param int $intId
   * Returns a PHP object representation of a container profile
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getContainerProfile($intId) {
    if(is_numeric($intId) && $intId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/container_profiles/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $container_profile = curl_exec($ch);
      curl_close($ch);
      if($container_profile != false) {
        $objContainerProfile = json_decode($container_profile);
      } else {
        throw new \Exception('Error: Could not query container profile');
      }
      return $objContainerProfile;
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

  /**
   * @return array
   * Returns an array with all location profile ids
   * @throws \Exception
   */
  public function getLocationProfiles() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/location_profiles?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $all_location_profiles = curl_exec($ch);
    curl_close($ch);
    if($all_location_profiles != false) {
      $arrAllLocationProfiles = json_decode($all_location_profiles);
    } else {
      throw new \Exception('Error: Could not query location profiles.');
    }

    return $arrAllLocationProfiles;
  }

  /**
   * @param int $intId
   * Returns a PHP object representation of a location profile
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getLocationProfile($intId) {
    if(is_numeric($intId) && $intId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/location_profiles/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $location_profile = curl_exec($ch);
      curl_close($ch);
      if($location_profile != false) {
        $objLocationProfile = json_decode($location_profile);
      } else {
        throw new \Exception('Error: Could not query location profile');
      }
      return $objLocationProfile;
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

  /**
   * @return array
   * Returns an array of all location ids
   * @throws \Exception
   */
  public function getLocations() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/locations?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $all_locations = curl_exec($ch);
    curl_close($ch);
    if($all_locations != false) {
      $arrAllLocations = json_decode($all_locations);
    } else {
      throw new \Exception('Error: Could not query locations.');
    }

    return $arrAllLocations;
  }

  /**
   * @param int $intId
   * Returns a PHP object representation of a location
   * @return mixed
   * @throws \Exception
   */
  public function getLocation($intId) {
    if(is_numeric($intId) && $intId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/locations/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $location = curl_exec($ch);
      curl_close($ch);
      if($location != false) {
        $objLocation = json_decode($location);
      } else {
        throw new \Exception('Error: Could not query location');
      }
      return $objLocation;
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

  /**
   * @return array
   * Returns an array of all repository ids
   * @throws \Exception
   */
  public function getRepositories() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $all_repositories = curl_exec($ch);
    curl_close($ch);
    if($all_repositories != false) {
      $mixedAllRepositories = json_decode($all_repositories);
      $arrAllRepositories = array();
      if(is_array($mixedAllRepositories)) {
        foreach($mixedAllRepositories as $objRepository) {
          $strRepositoryURI = $objRepository->uri;
          $arrRepositoryURI = explode('/', $strRepositoryURI);
          $arrAllRepositories[] = $arrRepositoryURI[2];
        }
      }
    } else {
      throw new \Exception('Error: Could not query repositories.');
    }

    return $arrAllRepositories;
  }

  /**
   * @param int $intId
   * Returns a PHP object representation of a repository
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getRepository($intId) {
    if(is_numeric($intId) && $intId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repository/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $repository = curl_exec($ch);
      curl_close($ch);
      if($repository != false) {
        $objRepository = json_decode($repository);
      } else {
        throw new \Exception('Error: Could not query repository');
      }
      return $objRepository;
    } else {
      throw new \InvalidArgumentException('$intId must be a positive numeric value');
    }
  }

  /**
   * @param int $intRepoId
   * Returns an array of all accessions for the selected repository
   * @return array
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getAccessions($intRepoId = 2) {
    if(is_numeric($intRepoId) && $intRepoId > 0) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/accessions?all_ids=true");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $all_accessions = curl_exec($ch);
      curl_close($ch);
      if ($all_accessions != FALSE) {
        $arrAllAccessions = json_decode($all_accessions);
      }
      else {
        throw new \Exception('Error: Could not query accessions.');
      }

      return $arrAllAccessions;
    } else {
      throw new \InvalidArgumentException('$intRepoId must be a positive numeric value');
    }
  }

  /**
   * @param int $intId
   * @param int $intRepoId
   * Returns a PHP object representation of an accession
   * @return mixed
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getAccession($intId, $intRepoId = 2) {
    if(filter_var($intId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      if (filter_var($intRepoId, FILTER_VALIDATE_INT, ['min_range' => 1])) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
        curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/accessions/$intId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $repository = curl_exec($ch);
        curl_close($ch);
        if ($repository != FALSE) {
          $objRepository = json_decode($repository);
        }
        else {
          throw new \Exception('Error: Could not query repository');
        }
        return $objRepository;
      }
      else {
        throw new \InvalidArgumentException('$intRepoId must be a positive integer');
      }
    } else {
      throw new \InvalidArgumentException('$intId must be a positive integer');
    }
  }

  /**
   * @param int $intRepoId
   * Returns an array of all archival object ids in the selected repository
   * @return array
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getArchivalObjects($intRepoId = 2) {
    if(filter_var($intRepoId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/archival_objects?all_ids=true");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $all_archival_objects = curl_exec($ch);
      curl_close($ch);
      if ($all_archival_objects != FALSE) {
        $arrAllArchivalObjects = json_decode($all_archival_objects);
      }
      else {
        throw new \Exception('Error: Could not query archival objects.');
      }

      return $arrAllArchivalObjects;
    } else {
      throw new \InvalidArgumentException('$intRepoId must be a positive integer');
    }
  }

  /**
   * @param int $intId
   * @param int $intRepoId
   * Returns a PHP object representation of an archival object
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getArchivalObject($intId, $intRepoId = 2) {
    if(filter_var($intId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      if (filter_var($intRepoId, FILTER_VALIDATE_INT, ['min_range' => 1])) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
        curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/archival_objects/$intId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $archival_object = curl_exec($ch);
        curl_close($ch);
        if ($archival_object != FALSE) {
          $objArchivalObject = json_decode($archival_object);
        }
        else {
          throw new \Exception('Error: Could not query archival object');
        }
        return $objArchivalObject;
      }
      else {
        throw new \InvalidArgumentException('$intRepoId must be a positive integer');
      }
    } else {
      throw new \InvalidArgumentException('$intId must be a positive integer');
    }
  }

  /**
   * @param int $intRepoId
   * Returns an array of all classification terms in the selected repository
   * @return array
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getClassificationTerms($intRepoId = 2) {
    if(filter_var($intRepoId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/classification_terms?all_ids=true");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $all_classification_terms = curl_exec($ch);
      curl_close($ch);
      if ($all_classification_terms != FALSE) {
        $arrAllClassificationTerms = json_decode($all_classification_terms);
      }
      else {
        throw new \Exception('Error: Could not query classification terms.');
      }

      return $arrAllClassificationTerms;
    } else {
      throw new \InvalidArgumentException('$intRepoId must be a positive integer');
    }
  }

  /**
   * @param int $intId
   * @param int $intRepoId
   * Returns a PHP object representation of a classification term
   * @return mixed
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getClassificationTerm($intId, $intRepoId = 2) {
    if(filter_var($intId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      if (filter_var($intRepoId, FILTER_VALIDATE_INT, ['min_range' => 1])) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
        curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/classification_terms/$intId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $classification_term = curl_exec($ch);
        curl_close($ch);
        if ($classification_term != FALSE) {
          $objClassificationTerm = json_decode($classification_term);
        }
        else {
          throw new \Exception('Error: Could not query classification term');
        }
        return $objClassificationTerm;
      }
      else {
        throw new \InvalidArgumentException('$intRepoId must be a positive integer');
      }
    } else {
      throw new \InvalidArgumentException('$intId must be a positive integer');
    }
  }

  /**
   * @param int $intRepoId
   * Returns an array of all classifications for the selected repository
   * @return array
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getClassifications($intRepoId = 2) {
    if(filter_var($intRepoId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/classifications?all_ids=true");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $all_classifications = curl_exec($ch);
      curl_close($ch);
      if ($all_classifications != FALSE) {
        $arrAllClassifications = json_decode($all_classifications);
      }
      else {
        throw new \Exception('Error: Could not query classification terms.');
      }

      return $arrAllClassifications;
    } else {
      throw new \InvalidArgumentException('$intRepoId must be a positive integer');
    }
  }

  /**
   * @param int $intId
   * @param int $intRepoId
   * Returns a PHP object representation of a classification
   * @return mixed
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getClassification($intId, $intRepoId = 2) {
    if(filter_var($intId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      if (filter_var($intRepoId, FILTER_VALIDATE_INT, ['min_range' => 1])) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
        curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/classifications/$intId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $classification = curl_exec($ch);
        curl_close($ch);
        if ($classification != FALSE) {
          $objClassification = json_decode($classification);
        }
        else {
          throw new \Exception('Error: Could not query classification');
        }
        return $objClassification;
      }
      else {
        throw new \InvalidArgumentException('$intRepoId must be a positive integer');
      }
    } else {
      throw new \InvalidArgumentException('$intId must be a positive integer');
    }
  }

  /**
   * @param int $intRepoId
   * Returns an array of all digital object ids for the selected repository
   * @return array
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getDigitalObjects($intRepoId = 2) {
    if(filter_var($intRepoId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/digital_objects?all_ids=true");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $all_digital_objects = curl_exec($ch);
      curl_close($ch);
      if ($all_digital_objects != FALSE) {
        $arrAllDigitalObjects = json_decode($all_digital_objects);
      }
      else {
        throw new \Exception('Error: Could not query digital objects.');
      }

      return $arrAllDigitalObjects;
    } else {
      throw new \InvalidArgumentException('$intRepoId must be a positive integer');
    }
  }

  /**
   * @param int $intId
   * @param int $intRepoId
   * Returns a PHP object representation of a digital object
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getDigitalObject($intId, $intRepoId = 2) {
    if(filter_var($intId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      if (filter_var($intRepoId, FILTER_VALIDATE_INT, ['min_range' => 1])) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
        curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/digital_objects/$intId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $digital_object = curl_exec($ch);
        curl_close($ch);
        if ($digital_object != FALSE) {
          $objDigitalObject = json_decode($digital_object);
        }
        else {
          throw new \Exception('Error: Could not query digital object');
        }
        return $objDigitalObject;
      }
      else {
        throw new \InvalidArgumentException('$intRepoId must be a positive integer');
      }
    } else {
      throw new \InvalidArgumentException('$intId must be a positive integer');
    }
  }

  /**
   * @param int $intRepoId
   * Returns an array of all top container ids for the selected repository
   * @return array
   * @throws \Exception
   */
  public function getTopContainers($intRepoId = 2) {
    if(filter_var($intRepoId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/top_containers?all_ids=true");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $all_top_containers = curl_exec($ch);
      curl_close($ch);
      if ($all_top_containers != FALSE) {
        $arrAllTopContainers = json_decode($all_top_containers);
      }
      else {
        throw new \Exception('Error: Could not query top containers.');
      }

      return $arrAllTopContainers;
    } else {
      throw new \InvalidArgumentException('$intRepoId must be a positive integer');
    }
  }

  /**
   * @param int $intId
   * @param int $intRepoId
   * Returns a PHP object representation of a top container
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getTopContainer($intId, $intRepoId = 2) {
    if(filter_var($intId, FILTER_VALIDATE_INT, array('min_range' => 1))) {
      if (filter_var($intRepoId, FILTER_VALIDATE_INT, ['min_range' => 1])) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
        curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/repositories/$intRepoId/top_containers/$intId");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $top_container = curl_exec($ch);
        curl_close($ch);
        if ($top_container != FALSE) {
          $objTopContainer = json_decode($top_container);
        }
        else {
          throw new \Exception('Error: Could not query top container');
        }
        return $objTopContainer;
      }
      else {
        throw new \InvalidArgumentException('$intRepoId must be a positive integer');
      }
    } else {
      throw new \InvalidArgumentException('$intId must be a positive integer');
    }
  }

  /**
   * Returns an array of all subject ids
   * @return array
   * @throws \Exception
   */
  public function getSubjects() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/subjects?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $all_subjects = curl_exec($ch);
    curl_close($ch);
    if ($all_subjects != FALSE) {
      $arrAllSubjects = json_decode($all_subjects);
    }
    else {
      throw new \Exception('Error: Could not query subjects.');
    }

    return $arrAllSubjects;
  }

  /**
   * @param int $intId
   * Returns a PHP object representation of a subject
   * @return object
   * @throws \Exception
   * @throws \InvalidArgumentException
   */
  public function getSubject($intId) {
    if (filter_var($intId, FILTER_VALIDATE_INT, ['min_range' => 1])) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/subjects/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $subject = curl_exec($ch);
      curl_close($ch);
      if ($subject != FALSE) {
        $objSubject = json_decode($subject);
      }
      else {
        throw new \Exception('Error: Could not query subject');
      }
      return $objSubject;
    }
    else {
      throw new \InvalidArgumentException('$intId must be a positive integer');
    }
  }

  /**
   * Returns an array of all vocabulary ids
   * @return array
   * @throws \Exception
   */
  public function getVocabularies() {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
    curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/vocabularies?all_ids=true");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $all_vocabularies = curl_exec($ch);
    curl_close($ch);
    if ($all_vocabularies != FALSE) {
      $arrAllVocabularies = json_decode($all_vocabularies);
    }
    else {
      throw new \Exception('Error: Could not query vocabularies.');
    }

    return $arrAllVocabularies;
  }

  /**
   * @param int $intId
   * Returns a PHP object representation of a vocabulary
   * @return object
   * @throws \Exception
   */
  public function getVocabulary($intId) {
    if (filter_var($intId, FILTER_VALIDATE_INT, ['min_range' => 1])) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HTTPHEADER, $this->arrHeader);
      curl_setopt($ch, CURLOPT_URL, $this->strBaseURL . "/vocabularies/$intId");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

      $vocabulary = curl_exec($ch);
      curl_close($ch);
      if ($vocabulary != FALSE) {
        $objVocabulary = json_decode($vocabulary);
      }
      else {
        throw new \Exception('Error: Could not query vocabulary');
      }
      return $objVocabulary;
    }
    else {
      throw new \InvalidArgumentException('$intId must be a positive integer');
    }
  }







}
